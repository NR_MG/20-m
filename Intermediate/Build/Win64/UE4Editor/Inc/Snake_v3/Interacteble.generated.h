// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_V3_Interacteble_generated_h
#error "Interacteble.generated.h already included, missing '#pragma once' in Interacteble.h"
#endif
#define SNAKE_V3_Interacteble_generated_h

#define Snake_v3_Source_Snake_v3_Interacteble_h_13_SPARSE_DATA
#define Snake_v3_Source_Snake_v3_Interacteble_h_13_RPC_WRAPPERS
#define Snake_v3_Source_Snake_v3_Interacteble_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_v3_Source_Snake_v3_Interacteble_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_V3_API UInteracteble(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteracteble) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_V3_API, UInteracteble); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteracteble); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_V3_API UInteracteble(UInteracteble&&); \
	SNAKE_V3_API UInteracteble(const UInteracteble&); \
public:


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	SNAKE_V3_API UInteracteble(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	SNAKE_V3_API UInteracteble(UInteracteble&&); \
	SNAKE_V3_API UInteracteble(const UInteracteble&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(SNAKE_V3_API, UInteracteble); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UInteracteble); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UInteracteble)


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUInteracteble(); \
	friend struct Z_Construct_UClass_UInteracteble_Statics; \
public: \
	DECLARE_CLASS(UInteracteble, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/Snake_v3"), SNAKE_V3_API) \
	DECLARE_SERIALIZER(UInteracteble)


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_GENERATED_UINTERFACE_BODY() \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IInteracteble() {} \
public: \
	typedef UInteracteble UClassType; \
	typedef IInteracteble ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_v3_Source_Snake_v3_Interacteble_h_13_INCLASS_IINTERFACE \
protected: \
	virtual ~IInteracteble() {} \
public: \
	typedef UInteracteble UClassType; \
	typedef IInteracteble ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Snake_v3_Source_Snake_v3_Interacteble_h_10_PROLOG
#define Snake_v3_Source_Snake_v3_Interacteble_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_SPARSE_DATA \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_RPC_WRAPPERS \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_v3_Source_Snake_v3_Interacteble_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_SPARSE_DATA \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_v3_Source_Snake_v3_Interacteble_h_13_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_V3_API UClass* StaticClass<class UInteracteble>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_v3_Source_Snake_v3_Interacteble_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
