// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake_v3/Interacteble.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeInteracteble() {}
// Cross Module References
	SNAKE_V3_API UClass* Z_Construct_UClass_UInteracteble_NoRegister();
	SNAKE_V3_API UClass* Z_Construct_UClass_UInteracteble();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_Snake_v3();
// End Cross Module References
	void UInteracteble::StaticRegisterNativesUInteracteble()
	{
	}
	UClass* Z_Construct_UClass_UInteracteble_NoRegister()
	{
		return UInteracteble::StaticClass();
	}
	struct Z_Construct_UClass_UInteracteble_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UInteracteble_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake_v3,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UInteracteble_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Interacteble.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UInteracteble_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IInteracteble>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UInteracteble_Statics::ClassParams = {
		&UInteracteble::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000840A1u,
		METADATA_PARAMS(Z_Construct_UClass_UInteracteble_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UInteracteble_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UInteracteble()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UInteracteble_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UInteracteble, 1250779982);
	template<> SNAKE_V3_API UClass* StaticClass<UInteracteble>()
	{
		return UInteracteble::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UInteracteble(Z_Construct_UClass_UInteracteble, &UInteracteble::StaticClass, TEXT("/Script/Snake_v3"), TEXT("UInteracteble"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UInteracteble);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
