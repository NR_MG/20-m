// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Snake_v3GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SNAKE_V3_API ASnake_v3GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
