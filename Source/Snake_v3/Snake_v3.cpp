// Copyright Epic Games, Inc. All Rights Reserved.

#include "Snake_v3.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Snake_v3, "Snake_v3" );
